'use strict';

function tools() {
	var video = $('#tools-anim');
	return {
		enter: function() {
			video.attr('src', 'images/tools-anim.webm');
			video.get(0).play();
		},
		leave: function() {
			video.attr('src', 'images/tools-anim-rev.webm');
			video.get(0).play();
		}
	}
}

function dncExample() {
	var container = document.getElementById('dnc-anim-container');
	var shadowSvg = Snap('#dnc-shadow');

	container.onload = function() {
		var svg = Snap(container.contentDocument.getElementById('dnc-anim'));

		var balls = [ svg.select('#ball') ];
		var shadow = shadowSvg.circle(balls[0].select('circle').asPX('cx'), balls[0].select('circle').asPX('cy'), 104);
		var shadowLayer = shadowSvg.group().add(shadow);
		var shadows = [ shadow ];
		var blurFilter = shadowSvg.filter(Snap.filter.blur(2));
		shadowLayer.attr({ opacity: 0.2, filter: blurFilter });
		shadowLayer.transform('t0,-20');

		balls[0].transformStack = [];
		shadows[0].transformStack = [];
		var horizontal = true;
		var desp = 150;
		var timeStep = 700;
		var ease = mina.easeinout;
		var animStepForward = function() {
			var n = balls.length;
			for(var j = 0; j < n; j++) {
				balls.push(balls[j].clone());
				var newShadow = shadows[j].clone();
				shadowLayer.add(newShadow);
				shadows.push(newShadow);
				balls[j+n].transformStack = [];
				shadows[j+n].transformStack = [];
			}
			for(var j = 0; j < n; j++) {
				balls[j  ].transformStack.push(balls[j  ].transform().string);
				balls[j+n].transformStack.push(balls[j+n].transform().string);
				shadows[j  ].transformStack.push(shadows[j  ].transform().string);
				shadows[j+n].transformStack.push(shadows[j+n].transform().string);
				if(horizontal) {
					balls[j  ].animate({ transform: balls[j  ].transform().string + 't'+desp+',0s0.75' }, timeStep, ease);
					balls[j+n].animate({ transform: balls[j+n].transform().string + 't-'+desp+',0s0.75' }, timeStep, ease);
					shadows[j  ].animate({ transform: shadows[j  ].transform().string + 't'+desp+',0s0.75' }, timeStep, ease);
					shadows[j+n].animate({ transform: shadows[j+n].transform().string + 't-'+desp+',0s0.75' }, timeStep, ease);
				} else {
					balls[j  ].animate({ transform: balls[j  ].transform().string + 't0,'+desp+'s0.75' }, timeStep, ease);
					balls[j+n].animate({ transform: balls[j+n].transform().string + 't0,-'+desp+'s0.75' }, timeStep, ease);
					shadows[j  ].animate({ transform: shadows[j  ].transform().string + 't0,'+desp+'s0.75' }, timeStep, ease);
					shadows[j+n].animate({ transform: shadows[j+n].transform().string + 't0,-'+desp+'s0.75' }, timeStep, ease);
				}
			}
			horizontal = !horizontal;
			desp *= 0.95;
		}

		var animStepBackward = function() {
			for(var j = 0; j < balls.length; j++) {
				balls[j].animate({ transform: balls[j].transformStack.pop() }, timeStep, ease);
				shadows[j].animate({ transform: shadows[j].transformStack.pop() }, timeStep, ease);
				var fill = balls[j].select('.fill');
				fill.animate({ opacity: parseFloat(fill.attr('opacity')) + 0.2 }, timeStep, ease);
			}
			setTimeout(function() {
				for(var i = balls.length-1; i >= balls.length/2; i--) {
					balls[i].remove();
					shadows[i].remove();
				}
				balls.length = balls.length / 2;
				shadows.length = shadows.length / 2;
			}, timeStep + timeStep/5);
		}

		var i = 0;
		slidestate.disableTransitions();
		var runAnim = function() {
			i++;
			if(i <= 4) {
				animStepForward();
			} else if(i == 5) {
				var mask = svg.select('#mask');
				var max = mask.asPX('width');
				svg.selectAll('.fill').animate({ opacity: 0.2 }, timeStep, ease);
				mask.animate({ height: max, y: mask.asPX('y') - max }, timeStep);
			} else if(i >= 6 && i <= 9) {
				animStepBackward();
			}
			if(i < 10)
				$(document).one('next', runAnim);
			else {
				slidestate.navigate.next();
				shadowSvg.clear();
				slidestate.enableTransitions();
			}
		}
		$(document).one('next', runAnim);
		$(document).one('prev', function() {
			$(document).off('prev');
			$(document).off('next');
			slidestate.navigate.prev();
			slidestate.enableTransitions();
			shadowSvg.clear();
		});
	}

	return {
		enter: function() {
			shadowSvg.clear();
		}
	}
} 

function dncComponents() {
	var subd = document.getElementById('dnc-desc-subd');
	var base = document.getElementById('dnc-desc-base');
	var redu = document.getElementById('dnc-desc-redu');
	var timeStep = 500;

	subd.onload = function() {
		var svg = Snap(subd.contentDocument.getElementById('dnc-anim'));
		var root = svg.select('#ball');
		var clone = root.clone();
		root.animate({ transform: 't-60,0s0.75' }, timeStep, mina.easeinout);
		clone.animate({ transform: 't60,0s0.75' }, timeStep, mina.easeinout);
	}

	base.onload = function() {
		var svg = Snap(base.contentDocument.getElementById('dnc-anim'));
		var root = svg.select('#ball');
		var clone = root.clone();
		root.transform('t-60,0s0.75');
		clone.transform('t60,0s0.75');
		var mask = svg.select('#mask');
		var max = mask.asPX('width');
		svg.selectAll('.fill').animate({ opacity: 0.5 }, timeStep, mina.easeinout);
		mask.animate({ height: max, y: mask.asPX('y') - max }, timeStep, mina.easeinout);
	}

	redu.onload = function() {
		var svg = Snap(redu.contentDocument.getElementById('dnc-anim'));
		var root = svg.select('#ball');
		var clone = root.clone();
		root.transform('t-60,0s0.75');
		clone.transform('t60,0s0.75');
		var mask = svg.select('#mask');
		var max = mask.asPX('width');
		svg.selectAll('.fill').attr({ opacity: 0.3 });
		mask.attr({ height: max, y: mask.asPX('y') - max });

		root.animate({ transform: 't0,0s1' }, timeStep, mina.easeinout);
		clone.animate({ transform: 't-0,0s1' }, timeStep, mina.easeinout, function() { clone.remove(); });
		svg.selectAll('.fill').animate({ opacity: 1 }, timeStep, mina.easeinout);
	}

	return {
		steps: {
			1: { in: function(el) { el.find('object').hide(); el.addClass('visible'); el.find('object').show(); } },
			2: { in: function(el) { el.find('object').hide(); el.addClass('visible'); el.find('object').show(); } },
			3: { in: function(el) { el.find('object').hide(); el.addClass('visible'); el.find('object').show(); } }
		}
	}
}

function showBarPlot(id) {
	var container = document.getElementById(id);

	return function() {
		container.onload = function() {
			var plot = Snap(container.contentDocument.querySelector('svg'));
			var bars = plot.selectAll('.bar');
			bars.forEach(function(bar) {
				bar.target = bar.asPX('height');
				bar.attr({ height: 0, y: bar.asPX('y') + bar.target });
				bar.animate({ height: bar.target, y: bar.asPX('y') - bar.target }, 1000, mina.easein);
			});
		}
	}
}

function parallelRecursionSequence() {
	var container = document.getElementById('parallel_recursion-sequence');

	container.onload = function() {
		var svg = Snap(container.contentDocument.querySelector('svg'));
		var sequence = svg.select('#sequence');
		var mask = svg.select('#mask');
		slidestate.disableTransitions();
		var block = 0;
		$(document).on('next', function() {
			block++;
			if(block <= 3) {
				sequence.animate({ transform: 't0,-'+(300*(block)) }, 500);
				mask.animate({ transform: 't0,'+(300*(block)) }, 500);
			}
			if(block > 3) {
				$(document).off('next');
				$(document).off('prev');
				slidestate.enableTransitions();
				slidestate.navigate.next();
			} 
		});

		$(document).on('prev', function() {
			block--;
			if(block < 0) {
				$(document).off('prev');
				$(document).off('next');
				slidestate.enableTransitions();
				slidestate.navigate.prev();
			}
			if(block >= 0) {
				sequence.animate({ transform: 't0,-'+(300*(block)) }, 500);
				mask.animate({ transform: 't0,'+(300*(block)) }, 500);
			}
		});
	}
}

function amorphousExample() {
	var container = document.getElementById('dmr-animation');

	container.onload = function() {
		var svg = Snap(container.contentDocument.querySelector('svg'));
		slidestate.disableTransitions();
		var step = 0;

		$(document).on('next', function() {
			step++;
			switch(step) {
				case 1:
					svg.selectAll('.bad').forEach(function(e, i) { setTimeout(function() { e.animate({ opacity: 1 }, 500); }, 500*i); });
					break;
				case 2:
					svg.selectAll('.neigh').animate({ opacity: 1 }, 500);
					break;
				case 3:
					svg.selectAll('.refined').animate({ opacity: 1 }, 500);
					break;
				case 4:
					$(document).off('next');
					$(document).off('prev');
					slidestate.enableTransitions();
					slidestate.navigate.next();
					break;
			}
		});

		$(document).on('prev', function() {
			step--;
			switch(step) {
				case -1:
					$(document).off('prev');
					$(document).off('next');
					slidestate.enableTransitions();
					slidestate.navigate.prev();
					break;
				case 0:
					svg.selectAll('.bad').forEach(function(e, i) { setTimeout(function() { e.animate({ opacity: 0 }, 500); }, 500*i); });
					break;
				case 1:
					svg.selectAll('.neigh').animate({ opacity: 0 }, 500);
					break;
				case 2:
					svg.selectAll('.refined').animate({ opacity: 0 }, 500);
					break;
			}
		});
	}
}

function workitem() {
	var container = document.getElementById('workitem-data');

	container.onload = function() {
		var svg = Snap(container.contentDocument.querySelector('svg'));

		setTimeout(function() {
			svg.select('#graph').animate({ transform: 't-324,-212s1.5,1.5'}, 500, mina.easeinout);
		}, 500);
		setTimeout(function() {
			svg.select('#indicator').animate({ opacity: 1 }, 300);
		}, 1000);
		svg.selectAll('#properties > text').forEach(function(e, i) {
			setTimeout(function() {
				e.animate({ opacity: 1 }, 200);
			}, 1500 + 100 * i);
		});
	}
}

function domainDecomp() {
	var container = document.getElementById('domain-decomp');

	container.onload = function() {
		slidestate.disableTransitions();
		var svg = Snap(container.contentDocument.querySelector('svg'));
		var sub1 = svg.select('#sub-1');
		var sub2 = svg.select('#sub-2');
		var lengthSub1 = sub1.asPX('x2');
		var lengthSub2 = sub2.asPX('y2');
		sub1.attr('x2', sub1.attr('x1'));
		sub2.attr('y2', sub2.attr('y1'));
		var step = 0;

		$(document).on('next', function() {
			step++;
			switch(step) {
				case 1:
					svg.select('#domain').animate({ opacity: 1 }, 300, mina.easeinout);
					break;
				case 2:
					sub1.animate({ x2: lengthSub1 }, 500, mina.easeinout);
					break;
				case 3:
					sub2.animate({ y2: lengthSub2 }, 500, mina.easeinout);
					break;
				case 4:
					svg.selectAll('.neigh').animate({ opacity: 1 }, 500);
					break;
				case 5:
					svg.selectAll('.refined').animate({ opacity: 1 }, 500);
					break;
				case 6:
					sub2.animate({ y2: sub2.attr('y1') }, 500, mina.easeinout);
					break;
				case 7:
					sub1.animate({ x2: sub1.attr('x1') }, 500, mina.easeinout);
					break;
				case 8:
					svg.select('#refined-last').animate({ opacity: 1 }, 500, mina.easeinout);
					break;
				case 9:
					$(document).off('next');
					$(document).off('prev');
					slidestate.enableTransitions();
					slidestate.navigate.next();
					break;
			}
		});

		$(document).on('prev', function() {
			step--;
			switch(step) {
				case -1:
					$(document).off('prev');
					$(document).off('next');
					slidestate.enableTransitions();
					slidestate.navigate.prev();
					break;
				case 0:
					svg.select('#domain').animate({ opacity: 0 }, 300, mina.easeinout);
					break;
				case 1:
					sub1.animate({ x2: sub1.attr('x1') }, 500, mina.easeinout);
					break;
				case 2:
					sub2.animate({ y2: sub2.attr('y1') }, 500, mina.easeinout);
					break;
				case 3:
					svg.selectAll('.neigh').animate({ opacity: 0 }, 500);
					break;
				case 4:
					svg.selectAll('.refined').animate({ opacity: 0 }, 500);
					break;
				case 5:
					sub2.animate({ y2: lengthSub2 }, 500, mina.easeinout);
					break;
				case 6:
					sub1.animate({ x2: lengthSub1 }, 500, mina.easeinout);
					break;
				case 7:
					svg.select('#refined-last').animate({ opacity: 0 }, 500, mina.easeinout);
					break;
			}
		});
	}
}

function clusteredDomain() {
	var container = document.getElementById('clustereddomain');

	container.onload = function() {
		var svg = Snap(container.contentDocument.querySelector('svg'));
		slidestate.disableTransitions();
		var step = 0;

		$(document).on('next', function() {
			step++;
			switch(step) {
			case 1:
				svg.select('#c01').animate({ fill: '#C12F3D' }, 300);
				svg.select('#c02').animate({ fill: '#669DED' }, 300);
				svg.select('#c03').animate({ fill: '#8FCC81' }, 300);
				svg.select('#c04').animate({ fill: '#CEB367' }, 300);
				break;
			case 2:
				svg.selectAll('#c01_1 *').animate({ fill: '#C12F3D' }, 300);
				svg.selectAll('#c02_1 *').animate({ fill: '#669DED' }, 300);
				svg.selectAll('#c03_1 *').animate({ fill: '#8FCC81' }, 300);
				svg.selectAll('#c04_1 *').animate({ fill: '#CEB367' }, 300);
				break;
			case 3:
				svg.selectAll('#c03_2 *').animate({ fill: '#8FCC81' }, 300);
				svg.selectAll('#c04_2 *').animate({ fill: '#CEB367' }, 300);
				break;
			case 4:
				$(document).off('next');
				$(document).off('prev');
				slidestate.enableTransitions();
				slidestate.navigate.next();
				break;
			}
		});

		$(document).on('prev', function() {
			$(document).off('prev');
			$(document).off('next');
			slidestate.enableTransitions();
			slidestate.navigate.prev();
		});
	};
}

function clusteredTreeDomain() {
	var container = document.getElementById('clusteredtreedomain');

	container.onload = function() {
		var svg = Snap(container.contentDocument.querySelector('svg'));
		slidestate.disableTransitions();
		var step = 0;

		$(document).on('next', function() {
			step++;
			switch(step) {
			case 1:
				svg.selectAll('#c01 *').animate({ fill: '#C12F3D' }, 300);
				svg.selectAll('#c02 *').animate({ fill: '#669DED' }, 300);
				break;
			case 2:
				svg.selectAll('#c012 *').animate({ fill: '#8FCC81' }, 300);
				svg.selectAll('#c022 *').animate({ fill: '#CEB367' }, 300);
				break;
			case 3:
				$(document).off('next');
				$(document).off('prev');
				slidestate.enableTransitions();
				slidestate.navigate.next();
				break;				
			}
		});

		$(document).on('prev', function() {
			$(document).off('prev');
			$(document).off('next');
			slidestate.enableTransitions();
			slidestate.navigate.prev();
		});		
	};
}

function domain2d() {
	var container = document.getElementById('domain2d');

	container.onload = function() {
		var svg = Snap(container.contentDocument.querySelector('svg'));
		var sub1 = svg.select('#sub-1');
		var sub2 = svg.select('#sub-2');
		var lengthSub1 = sub1.asPX('x2');
		var lengthSub2 = sub2.asPX('y2');
		sub1.attr('x2', sub1.attr('x1'));
		sub2.attr('y2', sub2.attr('y1'));

		setTimeout(function() {
			svg.select('#domain').animate({ opacity: 1 }, 300, mina.easeinout);
		}, 1000);
		setTimeout(function() {
					sub1.animate({ x2: lengthSub1 }, 500, mina.easeinout);
		}, 2000);
		setTimeout(function() {
					sub2.animate({ y2: lengthSub2 }, 500, mina.easeinout);
		}, 3000);
	};
}

function depIntroRisks() {
	var container = document.getElementById('risks-ex');

	container.onload = function() {
		var svg = Snap(container.contentDocument.querySelector('svg'));

		setTimeout(function() {
			svg.select('#background1-red').animate({ opacity: 1 }, 500);
		}, 1000);
		setTimeout(function() {
			svg.select('#background3-red').animate({ opacity: 1 }, 500);
		}, 2000);
	};
}

function depDependencies1() {
	var container = document.getElementById('deps-ex');

	container.onload = function() {
		var svg = Snap(container.contentDocument.querySelector('svg'));
		var step = 0;

		var x1 = svg.select('#declarations .guide').asPX('x1');
		var x2 = svg.select('#declarations .guide').asPX('x2');
		var y1 = svg.select('#invocations .guide').asPX('y1');
		var y2 = svg.select('#invocations .guide').asPX('y2');
		svg.select('#declarations .guide').attr('x2', x1);
		svg.select('#invocations .guide').attr('y2', y1);

		slidestate.disableTransitions();
		$(document).on('next', function() {
			step++;
			switch(step) {
				case 1:
					svg.select('#declarations .guide').animate({ x2: x2 }, 300);
					setTimeout(function() { svg.select('#declarations #label').animate({ opacity: 1 }, 300); }, 300);
					break;
				case 2:
					svg.select('#invocations .guide').animate({ y2: y2 }, 300);
					setTimeout(function() { svg.select('#invocations #label').animate({ opacity: 1 }, 300); }, 300);
					break;
				case 3:
					svg.selectAll('.arrow').animate({ opacity: 1 }, 300);
					break;
				case 4:
					$(document).off('next');
					$(document).off('prev');
					slidestate.enableTransitions();
					slidestate.navigate.next();
					break;
			}
		});
		$(document).on('prev', function() {
			step--;
			switch(step) {
				case -1:
					$(document).off('next');
					$(document).off('prev');
					slidestate.enableTransitions();
					slidestate.navigate.prev();
					break;
				case 0:
					svg.select('#declarations #label').animate({ opacity: 0 }, 300);
					setTimeout(function() { svg.select('#declarations .guide').animate({ x2: x1 }, 300); }, 300);
					break;
				case 1:
					svg.select('#invocations #label').animate({ opacity: 0 }, 300);
					setTimeout(function() { svg.select('#invocations .guide').animate({ y2: y1 }, 300); }, 300);
					break;
				case 2:
					svg.selectAll('.arrow').animate({ opacity: 0 }, 300);
					break;
			}
		});
	};
}

function depDependencies2() {
	var container = document.getElementById('deps-ex-2');

	container.onload = function() {
		var svg = Snap(container.contentDocument.querySelector('svg'));
		var step = 0;

		slidestate.disableTransitions();
		$(document).on('next', function() {
			step++;
			switch(step) {
				case 1:
					svg.selectAll('.second .mask').animate({ height: 150, transform: 't0,-150' }, 500);
					break;
				case 2:
					svg.select('#arrows .mask').animate({ height: 200, transform: 't0,-200' }, 500, mina.linear, function() {
						svg.selectAll('.first .mask').animate({ height: 150, transform: 't0,-150' }, 500);
					});
					break;
				case 3:
					$(document).off('next');
					$(document).off('prev');
					slidestate.enableTransitions();
					slidestate.navigate.next();
					break;
			}
		});
		$(document).on('prev', function() {
			step--;
			switch(step) {
				case -1:
					$(document).off('next');
					$(document).off('prev');
					slidestate.enableTransitions();
					slidestate.navigate.prev();
					break;
				case 0:
					svg.select('.second .mask').animate({ height: 0, transform: 't0,150' }, 500, mina.linear);
					break;
				case 1:
					svg.select('#arrows .mask').animate({ height: 0, transform: 't0,200' }, 500, mina.linear, function() {
						svg.selectAll('.first .mask').animate({ height: 0, transform: 't0,150' }, 500);
					});
					break;
			}
		});
	};
}

function close() {
	return {
		enter: function() {
			setTimeout(function() {
				$('#close').addClass('closing');
			}, 1000);
			var signature = $('<object id="signature" type="text/svg+xml" data="images/question.svg"></object>');
			signature.css({
				position: 'fixed',
				top: 0,
				left: 0,
				width: '100%',
				display: 'none'
			});
			$('body').append(signature);
			setTimeout(function() {
				signature.show();
				signature.load(function() {
					var draw = function(id) {
						var stroke = signature.get(0).contentDocument.getElementById(id);
						var length = stroke.getTotalLength();
						// Trigger a layout so styles are calculated & the browser
						// picks up the starting position before animating
						stroke.getBoundingClientRect();	
						stroke.style.WebkitTransition = 'stroke-dashoffset ' + (length/1500) + 's ease-in-out';
						stroke.style.strokeDashoffset = 0;
					};

					var parts = [ 'word', 't-cross', 'i-dot', 'question-mark', 'question-dot' ];
					parts.forEach(function(id) {
						var stroke = signature.get(0).contentDocument.getElementById(id);
						var length = stroke.getTotalLength();
						stroke.style.WebkitTransition = 'none';
						stroke.style.strokeDasharray = length + ' ' + length;
						stroke.style.strokeDashoffset = length;
					})
					var part = 0;
					var advance = function() {
						if(part < parts.length) {
							draw(parts[part]);
							signature.get(0).contentDocument.getElementById(parts[part]).addEventListener('webkitTransitionEnd', advance);
							part++;
						}
					};
					advance();
				});
			}, 2000)
		},
		leave: function() {
			$('#close').removeClass('closing');
			$('#signature').remove();
		}
	}
}

function sourceCodeHighlighter(lines) {
	var numbersToClasses = function(number) {
		return '.number' + number;
	};
	var selectors = [''].concat(lines.map(function(step) {
		if(step.length)
			return step.map(numbersToClasses).join(',')
		else
			return '.number' + step;
	}));
	var controller = function(slide) {
		var step = 0;
		slidestate.disableTransitions();
		$(document).on('next', function() {
			step++;
			slide.find(selectors[step-1]).removeClass('marked');
			if(step >= selectors.length) {
				$(document).off('next');
				$(document).off('prev');
				slidestate.enableTransitions();
				slidestate.navigate.next();
			} else {
				slide.find(selectors[step]).addClass('marked');
			}
		});
		$(document).on('prev', function() {
			step--;
			slide.find('*').removeClass('marked');
			if(step < 0) {
				$(document).off('next');
				$(document).off('prev');
				slidestate.enableTransitions();
				slidestate.navigate.prev();
			} else
				slide.find(selectors[step]).addClass('marked');
		});
	}

	return function() { return { enter: controller } };
}

$(function() {
	slidestate.init('pres');
	
	slidestate.plugin(progressBar, { active: false });
	slidestate.plugin(slideNumber);
	//slidestate.plugin(index);
	slidestate.plugin(mousewheel);
	slidestate.plugin(goTo);
	slidestate.plugin(goblin);

	slidestate.controller('intro-3', tools);
	slidestate.controller('dnc-example', dncExample);
	slidestate.controller('dnc-components', dncComponents);
	//slidestate.controller('skel-effort', showBarPlot('skel-effort-container'));
	//slidestate.controller('skel-cyclomatic', showBarPlot('skel-cyclomatic-container'));
	//slidestate.controller('skel-slocs', showBarPlot('skel-slocs-container'));
	slidestate.controller('parallel_recursion-desc', parallelRecursionSequence);

	slidestate.controller('parallel_recursion-api-01', sourceCodeHighlighter([ 3, 4, 5, 2, 10, 11, 12, 9 ]));
	slidestate.controller('parallel_recursion-api-01bis', sourceCodeHighlighter([ 2, 4, 13 ]));
	slidestate.controller('parallel_recursion-tbb-fib', sourceCodeHighlighter([ [5, 6, 7, 8], 15, 27, 20, 21, 24 ]));
	slidestate.controller('parallel_recursion-api-02', sourceCodeHighlighter([ 1, 2, 4, 8, 10, [14, 15] ]));
	slidestate.controller('amorphous-structure', sourceCodeHighlighter([ 1, 3, 5 ]));
	slidestate.controller('amorphous-code', sourceCodeHighlighter([ [2, 3], [5, 6, 7], 9, [10, 11], 12, 15, 16, 17 ]));

	slidestate.controller('amorphous-example', amorphousExample);
	slidestate.controller('amorphous-domain-01', workitem);
	slidestate.controller('amorphous-domain-03', domainDecomp);
	slidestate.controller('amorphous-config-04', clusteredDomain);
	slidestate.controller('amorphous-config-05', clusteredTreeDomain);
	slidestate.controller('amorphous-config-06', domain2d);

	slidestate.controller('depspawn-intro-risks', depIntroRisks);
	slidestate.controller('depspawn-deps-01', depDependencies1);
	slidestate.controller('depspawn-deps-02', depDependencies2);

	slidestate.controller('close', close);

	SyntaxHighlighter.defaults['toolbar'] = false;
	SyntaxHighlighter.all();
	
	slidestate.start('start');
});
